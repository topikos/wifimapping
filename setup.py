from setuptools import setup

setup(name='wifimapping',
      version='0.1',
      description='Create Maps from WiFi data',
      url='http://code.cs.uni-kassel.de',
      author='Bastian Schäfermeier',
      author_email='bsc@cs.uni-kassel.de',
      license='MIT',
      packages=['wifimapping'],
      install_requires=[
          'numpy',
          'pandas',
          'sklearn',
          'scipy',
          'matplotlib',
          'seaborn',
          'hdbscan',
          'pymongo'
      ],
      zip_safe=False)