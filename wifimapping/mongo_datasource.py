from pymongo import MongoClient
import pandas as pd
from datetime import datetime


def convert_iso_date(iso_date):
    """
    Example iso_date: "2019-05-24 18:30:00"
    The given date is assumed to be in the time zone of the machine where this program is running.
    The returned timestamp therefore is not necessarily the one in UTC.
    """
    return int(datetime.fromisoformat(iso_date).timestamp()*1000)


class MongoDataSource():

    def __init__(self, url=None):
        if url is None:
            url = 'mongodb://localhost:27017/'
        self.client = MongoClient(url)
        self.db = self.client.wally
        self.collection = self.db.dataPointCollection
        self.remove_duplicates = True
        self.remove_zero_rssi = True

    def fetch_day(self, date):
        """
        Returns the data which has been received by the server ata given date (e.g., a day).
        Important: This is different from the date, when the data was recorded.
        """
        start_of_day = convert_iso_date(date)
        end_of_day = start_of_day + 24*3600*1000
        # FIXME: fetch_interval expects Strings now.
        return self.fetch_interval(start_of_day, end_of_day)
    
    def fetch_interval(self, start, end):
        return self.fetch({"timestamp": {"$gt": convert_iso_date(start), "$lt": convert_iso_date(end)}})

    def fetch_starting_from(self, start_date):
        """
        Returns the data which has been received by the server at least at a given start date.
        Important: This is different from the date, when the data was recorded.
        """
        return self.fetch({"timestamp": {"$gt": convert_iso_date(start_date)}})

    def fetch(self, query):
        rows = []
        motionRows = []
        for doc in self.collection.find(query):
            uuid = doc['uuid']
            
            for point in doc['dataPoints']:    
                row = [uuid]
                
                # Point is WiFi data.
                if 'wifi' in point['channels']:
                    # Append timestamp
                    row.append(point['timestamp'])
                    row.append(point['offsetHours'])

                    # Append WiFi data
                    wifi = point['channels']['wifi']
                    row.append(wifi['ssid'])
                    row.append(wifi['bssid'])
                    row.append(wifi['rssi'])
                    row.append(wifi['frequency'])
                    
                    # Append the row
                    rows.append(row)
                    
                # Point is motion data
                elif 'motionSegment' in point['channels']:
                    
                    # Extract the data.
                    segment = point['channels']['motionSegment']
                    row.append(segment['start'])
                    row.append(segment['end'])
                    row.append(segment['motionMode'])
                    
                    # Append the row.
                    motionRows.append(row)
                    
                # Point is something else.
                else:
                    import pprint
                    print("Could not read DataPoint of unknown kind. Datapoint was:")
                    pprint.pprint(point)
                

        # ====== Init WiFi-Df.
        df = pd.DataFrame(rows, columns=['uuid', 'timestamp', 'offsetHours', 'ssid', 'bssid', 'rssi', 'frequency'])

        # Remove duplicates. A duplicate happens when everything, including the timestamp, is the same for two rows.
        # IMPORTANT: pandas does not consider the index for duplicate detection.
        # Therefore duplicate detection must be performed before removing the "timestamp" attribute-column
        # or the column must be kept.
        if self.remove_duplicates:
            print("Removing {} Duplicates".format(df.duplicated().sum()))
            df.drop_duplicates(inplace=True)

        # TODO: Add the time offset if necessary
        df['timestamp'] = pd.to_datetime(df['timestamp'], unit='ms')
        df.set_index('timestamp', drop=True, inplace=True)

        # Remove 0 rssi values.
        if self.remove_zero_rssi:
            zeros = (df['rssi'] == 0)
            print("Removing {} zero-values".format(zeros.sum()))
            df = df.loc[df['rssi'] != 0]
            print("Final dataframe size: {}".format(df.shape))

        # ====== Init Motion-Df.
        motionDf = pd.DataFrame(motionRows, columns=['uuid', 'start', 'end', 'motionMode'])
        motionDf['start'] = pd.to_datetime(motionDf['start'], unit='ms')
        motionDf['end'] = pd.to_datetime(motionDf['end'], unit='ms')
        motionDf['duration'] = motionDf['end'] - motionDf['start']

        return df, motionDf


if __name__ == "__main__":
    datasource = MongoDataSource('mongodb://localhost:27017/')
    #wifiDf, motionDf = datasource.fetch_day("2019-05-30")
    #wifiDf, motionDf = datasource.fetch_interval("2019-05-26 00:00:00", "2019-05-29 12:30:00")
    wifiDf, motionDf = datasource.fetch_starting_from("2019-05-26")
    print(wifiDf.head())
    print(motionDf.head())
