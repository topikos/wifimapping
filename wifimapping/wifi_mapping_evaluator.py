import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def get_location_pairs_with_gt(segments, get_gt_function=lambda segment: None if len(segment.new_gt) == 0 else segment.new_gt[0]):
    """
    Takes a list of segments (must be a list to ensure iteration order and meaningful results).
    Returns a dictionary with one set of segment pairs from different locations and one set of pairs from same locations.
    The pairs are the indices of the segments in the given list of segments. 
    """
    
    # Init map which contains a set of pairs of segment indices for segments from the same and segments from different locations.
    same_locations, different_locations = 'same_locations', 'different_locations'
    result = {same_locations: set(), different_locations: set()}

    # Fill the sets of location pairs.
    for i, segmentA in enumerate(segments):
        for j, segmentB in enumerate(segments):

            # Ignore pairs where segmentA = segmentB
            if i == j: continue

            # Retrieve ground truth for both segments.
            gtA, gtB =  get_gt_function(segmentA), get_gt_function(segmentB)

            # If we have ground truth for both segments, determine whether they are from same or from different locations and add the tuple accordingly.
            if gtA is not None and gtB is not None:
                set_to_add = same_locations if gtA == gtB else different_locations
                tuple_to_add = tuple(sorted((i, j)))
                result[set_to_add].add(tuple_to_add)
    return result


def evaluate_distance_matrix(distance_mat, distance_name, same_location_pairs, different_location_pairs, plot_results=False):
    from sklearn.metrics import roc_curve, roc_auc_score, auc
    import matplotlib.pyplot as plt

    # Determine true labels from RFIDs and distances for all place pairs which have labels.
    y_true = []
    y_score = []
    for i,j in same_location_pairs:
        d = distance_mat[i][j]
        true_label = 0
        y_true.append(true_label)
        y_score.append(d)

    for i,j in different_location_pairs:
        d = distance_mat[i][j]
        true_label = 1
        
        y_true.append(true_label)
        y_score.append(d)

    # Determine false positive rate and true positive rate at different distance thresholds.
    # These define the ROC curve.
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    auc = roc_auc_score(y_true, y_score)

    # Plot the ROC curve.
    if plot_results:
        plt.figure()
        plt.title('ROC Curve for {} (AUC={:.2f})'.format(distance_name, auc))
        #plt.suptitle('AUC={}'.format(auc))
        plt.xlabel('FPR')
        plt.ylabel('TPR')
        plt.plot(fpr, tpr)
        plt.show()
    
    # Return Area under the ROC curve and the ROC curve.
    return {'auc': auc, 'fpr': fpr, 'tpr': tpr, 'thresholds': thresholds}

def plot_roc_curve(evaluation_results, uuid, save=False):
    ''' Plot ROC Curves for all evaluation_results obtained through the method evaluate_distance_matrix. '''
    
    plt.figure(figsize=(5,5))
    plt.title('ROC Curves of Distance Measures')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    
    auc_sorted_results = sorted(evaluation_results.items(), key=lambda x: x[1]['auc'], reverse=True)
    for dist_name, results in auc_sorted_results:
        plt.plot(results['fpr'], results['tpr'], label='{}, AUC={:.3f}'.format(dist_name, results['auc']))
    ax = plt.gca()
    #plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
    plt.plot((0,1), (0,1), ls="--", c=".3")
    plt.legend()
    plt.show()
    
    if save:
        plt.savefig('./Distance Evaluation/roc_{}.pdf'.format(uuid))

def plot_distance_relationship(boxplot, boxplot_values, boxplot_positions, save_figure=False, uuid=None):
    """ If boxplot is set to true, make a boxplot, otherwise a scatterplot. Both plots will be saved if save_figure is True. 
    boxplot_values is a two dimensional array, where each element at index i contains the values for boxplot_positions[i]."""
    from matplotlib.ticker import FormatStrFormatter

    plt.figure()
    if boxplot:
        plt.boxplot(boxplot_values, positions=boxplot_positions)#, widths=0.1)
        plt.xticks(boxplot_positions, rotation='vertical')
        filename_part = 'boxplot'
    else:
        plt.scatter(boxplot_positions, [np.mean(y) for y in boxplot_values])
        filename_part = 'scatter'

    plt.xlabel('Euclidean Distance in Floorplan')
    plt.ylabel('Calculated Distance')
    plt.title('Floorplan Distances and Calculated Distances Between WiFi Distributions')
    plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%.2f'))#'%g'))
    plt.show()
    if save_figure:
            plt.savefig('./Distance Evaluation/distance_{}_{}.pdf'.format(filename_part, uuid))


def determine_tag_distances():
    ''' Determine distances between location tags. Returns a pair tag_distances, tag_indices. tag_indices can be used to lookup the index of a tag name (string)
    in the matrix tag_distances. '''
    tag_names = ['locA', 'locB', 'locC', 'locD', 'locE', 'locF', 'locG', 'locH', 'locI', 'locJ', 'locK', 'locL'] 
    tag_indices = dict([(tag_names[i], i) for i in range(len(tag_names))])
    # Coordinates as determined from a floorplan. The units were measured in cm, but we only care about the ratios of the distances.
    tag_coordinates = np.array([[0.8, 0.7], [1.8, 1.2], [3.5, 1.0], [5.5, 1.0], [6, 3.5], [4.7, 4], [2.1, 4.6], [2.1, 3.9], [1.4, 3.7],
                    [0.5, 9.4], [0.5, 10.7], [3.75, 11.85]])
    tag_distances = [[np.linalg.norm(tag_coordinates[i,:] - tag_coordinates[j,:]) for j in range(len(tag_coordinates))] for i in range(len(tag_coordinates))]
    tag_distances = np.array(tag_distances)
    return tag_distances, tag_indices

def get_floorplan_calculated_distance_pairs(m, segmentpairs, tag_distances, tag_indices):
    ''' Returns a two-dimensional array where each row is a pair [tag_distance, calculated_distance]. 
    tag_distance is the distance between tags in a floorplan. calculated_distance is the calculated distance between the segment distributions. '''

    # Start determining
    distance_pairs = []
    reverse_gt_locations = dict([(val, key) for key, val in m.gt_locations_map.items()])
    for i,j in segmentpairs:

        # Get the calculated segment distance.
        calculated_dist = m.segment_distances[i,j]

        # Method to retrieve ground truth label (tag name) for a segment index.
        get_gt = lambda index: reverse_gt_locations[m.wifi_segments[index].new_gt[0]]

        # Retrieve tag names and index in the tag-coordinates array.
        tag_i, tag_j = tag_indices[get_gt(i)], tag_indices[get_gt(j)]

        # Retrieve the floorplan distance.
        tag_distance = tag_distances[tag_i, tag_j]

        # Add the distance pair.
        distance_pairs.append([tag_distance, calculated_dist])

    distance_pairs = np.array(distance_pairs)
    return distance_pairs

def calc_distance_correlation(mapper, loc_same, loc_diff, plot_relations=True, save_figure=True):
    ''' Calculate correlation coefficients between floorplan and calculated distances. '''
    from scipy.stats import spearmanr, kendalltau, pearsonr
    
    tag_distances, tag_indices = determine_tag_distances()

    # Get distance pairs for same and different locations
    same = get_floorplan_calculated_distance_pairs(mapper, loc_same, tag_distances, tag_indices)
    diff = get_floorplan_calculated_distance_pairs(mapper, loc_diff, tag_distances, tag_indices)
    
    # Put both together in a DataFrame
    pair_df = pd.concat([pd.DataFrame(same), pd.DataFrame(diff)])
    
    # Calculate correlation coefficients.
    spearman = spearmanr(pair_df[0], pair_df[1])
    pearson = pearsonr(pair_df[0], pair_df[1])
    kendall = kendalltau(pair_df[0], pair_df[1])
    
    # Plot scatter and boxplot of distance relations
    if plot_relations:
        # Make an array of x values and an array of arrays for y values for plotting.
        boxplot_positions = []
        boxplot_values = []
        for xvalue, g in pair_df.groupby(0):
            boxplot_values.append(g[1].values)
            boxplot_positions.append(xvalue)
            
        # Plot the relationship. First a boxplot, then a scatterplot of the y-means.
        plot_distance_relationship(True, boxplot_values, boxplot_positions, save_figure=save_figure, uuid=mapper.uuid)
        plot_distance_relationship(False, boxplot_values, boxplot_positions, save_figure=save_figure, uuid=mapper.uuid)

    return {'spearman': spearman, 'pearson': pearson, 'kendall': kendall}


def plot_segment_durations(segments, bins=100):
    ''' Plot the durations of segments on a log scale. '''
    durations = [segment.get_duration().total_seconds() for segment in segments]
    plt.figure()
    plt.xscale('log')
    #plt.xticks([10^x for x in range(4)])
    plt.title('Distribution of Segment Durations')
    plt.hist(durations, bins=bins)
    plt.show()

def plot_dist_hist(m, dist_name, loc_same, loc_diff, nbins=100, save_figure=True):
    ''' Plot histograms of calculated distances for same locations and different locations.
    loc_same and loc_diff are the location pairs as returned by get_location_pairs_with_gt. '''
    def get_mat_values(mat, index_set, filter_finite=True):
        result = [mat[i,j] for i,j in index_set]
        if filter_finite:
            result = list(filter(lambda x: np.isfinite(x), result))
        return result
    
    plt.figure()
    plt.title(dist_name)
    plt.xlabel('Distance')
    plt.ylabel('Density')
    plt.hist(get_mat_values(m.segment_distances, loc_same), label='Same Locations', normed=True, bins=nbins, alpha=0.6)
    plt.hist(get_mat_values(m.segment_distances, loc_diff), label='Different Locations', normed=True, bins=nbins, alpha=0.6)
    plt.legend()
    plt.show()
    if save_figure:
        plt.savefig('./Distance Evaluation/hist_{}_{}.pdf'.format(dist_name, m.uuid))