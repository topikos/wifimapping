import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def plot_wifi_and_motion(df, motionDf, uuid, date_to_plot, with_legend=False):
    import matplotlib.pyplot as plt
    plt.figure()
    
    # Plot WiFi series
    for name, group in df.loc[df['uuid'] == uuid][date_to_plot].groupby(['ssid', 'bssid']):
        group['num_val'].plot(label=name, marker='x')
    
    # Plot motion segments
    for index, row in motionDf.loc[motionDf['uuid'] == uuid].iterrows():
        motionColor = 'r' if row.motionMode == 'STATIC' else 'b'
        #plt.hlines(y=-20+3*(0 if row.motionMode == 'STATIC' else 1), xmin=row.start, xmax=row.end, color=motionColor, linewidth=10)
        plt.hlines(y=-20, xmin=row.start, xmax=row.end, color=motionColor, linewidth=10, alpha=0.5)
        
    if with_legend:
        plt.legend()
        
    plt.show()

def create_wifi_df(df):
    g2 = df.groupby(('sensor', 'comp')).get_group(('Wifi', 'sigLevel'))

    # Join the sigLevel-rows of the DataFrame on corresponding ssid and bssid rows.
    bssids = df.loc[(df['sensor'] == 'Wifi') & (df['comp'] == 'bssid'), ('sample_id', 'str_val')]
    ssids = df.loc[(df['sensor'] == 'Wifi') & (df['comp'] == 'ssid'), ('sample_id', 'str_val')]
    bssids.rename(columns={'str_val': 'bssid'}, inplace=True)
    ssids.rename(columns={'str_val': 'ssid'}, inplace=True)
    wifi = pd.merge(g2, bssids, on='sample_id', how='left')
    wifi = pd.merge(wifi, ssids, on='sample_id', how='left')

    # Index will be gone after merge. Set again in case we have used a datetime index.
    wifi.set_index(bssids.index, inplace=True)
    
    # "Fix" broken categories. Otherwise all values from the original "str_val" column will be allowed category values.
    # This would, e.g. break behaviour of groupby.
    for col in ['bssid', 'ssid']:
        wifi[col] = wifi[col].astype('str').astype('category')

    # Remove unused columns
    for column in ['sensor', 'comp', 'str_val', 'ts']:
        del wifi[column]

    # Convert all values to string
    wifi['bssid']=wifi['bssid'].apply(str)
    wifi['ssid']=wifi['ssid'].apply(str)
    return wifi

def load_data(fullpath):
    """ Loads a data set from CSV. """
    df = pd.read_csv(fullpath, encoding='utf-16', parse_dates=['dt'], index_col='dt')
    df.sort_values(by='ts', inplace=True)
    return df

def series_for(df, uuid, sensor, comp=None):
    """ Returns a time series for the given uuid, sensor and sensor component.
    If no component is provided, a dict is returned with one series for each component.
    TODO: does not work for string values
    """
    df = df.loc[df['uuid'] == uuid]
    grouped = df.groupby(['uuid', 'sensor', 'comp'])
    comps = df.loc[df['sensor'] == sensor, 'comp'].unique() if comp is None else [comp]
    comp_series = dict()
    for comp in comps:
        g = grouped.get_group((uuid, sensor, comp))
        comp_series[comp] = (pd.Series(g['num_val'].values, index=g['dt'], name='{} {} (UUID: {})'.format(sensor, comp, uuid)))
    return next(iter(comp_series.values())) if len(comp_series) == 1 else comp_series

def timestamp_diffs(df):
    """ Returns differences between consecutive timestamps. """
    ts = df.index.unique().values.copy()
    ts2 = ts.copy()
    ts = np.delete(ts, 0)
    ts2 = np.delete(ts2, -1)
    diff = (ts - ts2)
    diff /= 1e6 # Convert to milliseconds
    return diff.astype('int')

def timestamp_diff_summary(df):
    """ Returns summary statistics about the difference betweeen consecutive timestamps.
    Useful for getting an overview about data and finding possible problems. """
    return pd.Series(timestamp_diffs(df)).describe()