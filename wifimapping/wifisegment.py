import math
from collections import Counter
from scipy.stats import wasserstein_distance
import numpy as np
import pandas as pd


class WifiSegment():
    
    def __init__(self, fingerprints, bssids, distribution_types='pmf'):
        ''' Init this WifiSegment.
        distribution_types determines how RSSI distributions will be estimated and can be either 'pmf', 'kde', or 'normal'.
        '''
        self.bssids = bssids
        self.counts = dict()
        self.timeframe = (fingerprints.X.index.min(), fingerprints.X.index.max())
        self.n_measurements = 0
        self.__count_occurences(fingerprints)
        self.cum_dist = None
        self.normalized_occ = None
        self.cum_dist_matrix = None
        self.dist_matrix = None
        self.distribution_types = distribution_types
    
    def get_duration(self):
        return self.timeframe[1] - self.timeframe[0]
    
    def mean(self, bssid):
        p = self.normalized_occurences()[bssid]
        expectation = sum([p[rssi]*rssi for rssi in p])
        return expectation
    
    def variance(self, bssid):
        p = self.normalized_occurences()[bssid]
        expectation = self.mean(bssid)
        variance = sum(np.power(rssi - expectation, 2) for rssi in p)/len(p)
        return variance
    
    def stddev(self, bssid):
        return np.sqrt(self.variance(bssid))
    
    def feature_vector(self):
        result = []
        for bssid in self.bssids:
            result.append(self.mean(bssid))
            result.append(self.stddev(bssid))
        return np.array(result)
    
    def visible_accesspoints(self):
        return {bssid for bssid in self.counts.keys() if len(self.counts[bssid]) > 1}
    
    def __count_occurences(self, fingerprints):

        self.counts = dict()
        self.n_measurements = 0
        df = fingerprints.X

        for bssid in self.bssids:
            rssi_series = pd.Series() if bssid not in df else df.loc[:, bssid]

            # Count rssi-values.
            counts_series = rssi_series.value_counts()
            counter = Counter()
            for rssi, count in counts_series.items():
                counter[rssi] = count
                self.n_measurements += 1
            
            # Bssid invisible. Add "minimal" dummy rssi.
            # Probability of observing the minimal rssi is 1.
            if len(counter) == 0:
                counter[-100] = 1

            # Add to resulting dictionary.
            self.counts[bssid] = counter
        return self.counts
    
    def normalized_occurences(self):
        if self.normalized_occ is not None:
            return self.normalized_occ
        
        result = self.counts.copy()
        for bssid in result:
            result[bssid] = self.counts[bssid].copy()
            histogram = result[bssid]
            total_count = sum(histogram.values())
            for rssi in histogram:
                histogram[rssi] /= total_count
        self.normalized_occ = result
        return result
    
    def cumulative_distribution(self):
        if self.cum_dist is not None:
            return self.cum_dist
        
        result = self.normalized_occurences().copy()
        for bssid, counter in result.items():
            cum_dist = Counter()
            cum_sum = 0.0
            for rssi, count in sorted(counter.items()):
                cum_sum += count
                cum_dist[rssi] = cum_sum
            result[bssid] = cum_dist
        self.cum_dist = result
        return result
    
    def plot_distributions(self, dist_type='pdf', n_cols=8):
        ''' Plots all signal distributions from this segment.
        dist_type can be either "pdf" (probability density/mass function) or "cdf" for cumulative probabilities. '''
        import matplotlib.pyplot as plt

        dist_mat = self.get_distribution_matrix() if dist_type == 'pdf' else self.get_cum_dist_matrix()
        n_figures = dist_mat.shape[0]
        n_rows = int(np.ceil(n_figures / n_cols))

        plt.figure()
        for i in range(n_figures):
            plt.subplot(n_rows, n_cols, i+1)
            plt.ylim((0,1))
            plt.plot(dist_mat[i,:])
        plt.show()

    def joint_log_probability(self, x_values):
        ''' Returns the joint log probability of the given rssi values assuming independence of the access point signal strengths. 
        Parameters
        ----------
        x_values : dict
            bssids mapped to the rssi values for which to evaluate the joint probability.
        Returns
        -------
        float
            the joint log probability of the given rssi values.
        '''
        log_prob = 0.0
        p = self.normalized_occurences()
        for bssid, rssi in x_values.items():
            log_prob += np.log(p[bssid][rssi])
        return log_prob
    
    def average_kl_divergence(self, other):
        import math
        
        # TODO:
        # - If q is zero, p must be zero or otherwise KL is undefined.
        # - Instead, if one of either p or q is zero, we set the summand to zero.
        # - If we have two distributions where either is zero
        # - If we use kernel density estimation with gaussians instead, we don't have
        #   this problem since there cannot occur any zero probabilities.
        
        def kl_divergence(p, q):
            """ Calculates the Kullback-Leibler-Divergence between discrete distributions p and q.
            The distributions p and q are maps with outcomes as keys and their respective probabilities as values.
            Summands for outcomes x where either p(x) or q(x) is zero are assumed to be zero.
            """
            result = 0.0
            outcomes = set(p.keys()).intersection(q.keys())
            for outcome in outcomes:
                p_x, q_x = p[outcome], q[outcome]
                if p_x != 0 and q_x != 0:
                    result += p_x*(math.log(p_x)-math.log(q_x))
            return result
        
        def symmetric_kl_divergence(p, q):
            ''' Symmetric version of KL-Divergence KL2(p,q) = KL(p||q) + KL(q||p)'''
            return kl_divergence(p, q) + kl_divergence(q, p)
        
        # Calculate normalized rssi-distributions for all access points.
        p = self.normalized_occurences()
        q = other.normalized_occurences()
        
        # Calculate the average symmetric KL-Divergence of the access points in both sets..
        #bssids = set(p.keys()).union(q.keys())
        result = 0.0
        n = 0
        for bssid in self.bssids:
            if bssid not in p or bssid not in q:
                continue
            p_1, q_1 = p[bssid], q[bssid]
            
            # If the access point was visible in both segments, we add their KL-Divergence to the sum.
            # This is somewhat problematic, since we remove the information that these distributions actually differ
            # if only one of both was visible/invisible.
            if len(p_1) != 0 and len(q_1) != 0:
                result += symmetric_kl_divergence(p[bssid], q[bssid])
                n += 1
        return result / n if n>0 else float("inf")

    def get_cum_dist_matrix(self):
        '''
        Converts the cumulative signal distributions of each access point to a matrix.
        Each row denotes an access point (ordered by bssid) and each column an rssi-value (from -100 to -11).
        '''
        if self.cum_dist_matrix is not None:
            return self.cum_dist_matrix

        cum_dist = self.cumulative_distribution()

        rssi_values = range(-100, -10)
        cum_dist_matrix = np.zeros((len(self.bssids), len(rssi_values)))

        for i, bssid in enumerate(self.bssids):
            previous_p = 0.0
            current_cumdist = cum_dist[bssid]
            for j, rssi_value in enumerate(rssi_values):
                current_p = current_cumdist[rssi_value] if rssi_value in current_cumdist else previous_p
                cum_dist_matrix[i, j] = current_p
                previous_p = current_p
        
        self.cum_dist_matrix = cum_dist_matrix
        return cum_dist_matrix
    
    def univariate_kde(self, samples, bandwidth=2, rssi_values=range(-100, -10)):
        ''' Performs Gaussian kernel density estimation for a given array of samples (observed RSSI values).
        '''
        from sklearn.neighbors import KernelDensity

        # KDE takes an array of samples where each sample is an array (in our case 1-dimensional because univariate). 
        X = np.array(samples).reshape((-1,1))
        kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(X)

        # Same array shape when evaluating log-probabilities.
        x_vals = np.array(rssi_values).reshape((-1,1))
        log_density = kde.score_samples(x_vals)

        # Convert log-densities to densities and normalize.
        density = np.exp(log_density)
        density = density/density.sum()

        return density

    def get_distribution_matrix(self):
        '''
        Converts the (non-cumulative) signal distributions of each access point to a matrix.
        Each row denotes an access point (ordered by bssid) and each column an rssi-value (from -100 to -11).
        TODO: This is the only representation of distributions that we still use.
        Cumulative distributions are now calculated directly from the distance measure. Clean out the other stuff
        (we can directly determine distribution from the input fingerprint matrix) and also remove the distance measures from here. 
        '''
        if self.dist_matrix is not None:
            return self.dist_matrix

        dist = self.normalized_occurences()

        rssi_values = range(-100, -10)
        dist_matrix = np.zeros((len(self.bssids), len(rssi_values)))

        for i, bssid in enumerate(self.bssids):
            current_dist = dist[bssid]
            
            # Estimate as probability mass function through relative frequencies.
            if self.distribution_types == 'pmf':
                for j, rssi_value in enumerate(rssi_values):
                    current_p = current_dist[rssi_value] if rssi_value in current_dist else 0.0
                    dist_matrix[i, j] = current_p
            # Estimate through kernel density estimation with Gaussian kernel.
            elif self.distribution_types == 'kde':
                samples = [[rssi_value] for rssi_value, count in current_dist.items() for _ in range(int(count*self.n_measurements))]
                dist_matrix[i,:] = self.univariate_kde(samples, rssi_values=rssi_values)
            # Estimate as normal distribution. We do this by calculating the sample mean and variance.
            # Then we perform KDE with a single sample located at the mean and with the bandwith set to the variance.
            elif self.distribution_types == 'normal':
                samples = [[rssi_value] for rssi_value, count in current_dist.items() for _ in range(int(count*self.n_measurements))]
                mean = np.mean(samples)

                # ddof=1 calculates sample variance (with denominator n-1).
                # According to numpy doc, for normally distributed data one can also take ddof=0.
                bandwidth = np.var(samples, ddof=1)

                # A special case arises, when we have a single sample, where we either have variance 0 or sample variance nan.
                # When all samples have the same value, we also have variance 0 and set it to the default bandwith instead.
                if len(samples) == 1 or bandwidth == 0:
                    bandwidth = 5
                dist_matrix[i,:] = self.univariate_kde([[mean]], bandwidth=bandwidth, rssi_values=rssi_values)
            else:
                raise Exception('Cannot estimate distribution. The distribution_types argument "{}" is unknown.'.format(self.distribution_types))

        
        self.dist_matrix = dist_matrix
        return dist_matrix

    def earth_movers_distance(self, other):
        # We first build the sum of the absolute differences for each column (divided by the number of columns),
        # Then sum these up for each row (divided by the number of rows). This is the same as dividing the sum of all matrix entries
        # at the end by the number of rows and cols (the number of entries in the matrix).
        mat1, mat2 = self.get_cum_dist_matrix(), other.get_cum_dist_matrix()
        result = np.absolute(mat1-mat2).sum()
        result /= mat1.size
        return result

    def total_variance(self, other, jaccard_weighted=False, wasserstein=False):
        """ Returns a tuple (d1, d2).
        d1 is the sum of the test statistic from a Kolmogorov Smirnoff test.
        The test is performed between the distributions of all pairs of the same access point. 
        d2 is the sum for the so-called total variance, which is calculated similarly to the Kolomogorov Smirnoff test. 
        The test computes the maximum difference between two cumulative distributions (Kolmogorov Smirnoff) 
        or the average distance (total variance).
        """
        if wasserstein:
            p = self.normalized_occurences()
            q = other.normalized_occurences()
        else:
            p = self.cumulative_distribution()
            q = other.cumulative_distribution()
        
        avg_max, avg_avgdifference = 0.0, 0.0
        #bssid_intersection = self.visible_accesspoints().intersection(other.visible_accesspoints())
        bssid_union = self.visible_accesspoints().union(other.visible_accesspoints())
        jaccard_metric = self.jaccard_metric(other)
        #print(jaccard_metric)
        #for bssid in bssid_union:
        for bssid in self.bssids:
            
            # If the bssid was never visible for one of the pdfs, one of both is actually no real pdf
            # TODO: what should we do with this case?
            
            # For invisible access points we have two cases:
            # 1. Invisible in both segments => Added distance is zero
            if bssid not in p and bssid not in q:
                continue
            # 2. Invisible for only one segment => Assume probability of minimal rssi is 1.
            if bssid not in p and bssid in q:
                p[bssid] = Counter()
                p[bssid][-100] = 1.0
            if bssid not in q and bssid in p:
                q[bssid] = Counter()
                q[bssid][-100] = 1.0
            # TODO: we can also just check for both cdfs whether bssid is contained and if not, set it to -100.
            
            cdf1, cdf2 = p[bssid], q[bssid]
            max_difference, avg_difference = 0.0, 0.0
            #rssi_values = sorted(set(cdf1.keys()).union(cdf2.keys()))
            rssi_values = range(-100,-10)
            
            if wasserstein:
                u_weights = [cdf1[rssi] for rssi in rssi_values]
                v_weights = [cdf2[rssi] for rssi in rssi_values]
                avg_difference += wasserstein_distance(rssi_values, rssi_values, u_weights, v_weights)
            else:
                prev_cum_val1, prev_cum_val2 = 0.0, 0.0
                for rssi in rssi_values:

                    cum_val1 = cdf1[rssi] if rssi in cdf1 else prev_cum_val1
                    cum_val2 = cdf2[rssi] if rssi in cdf2 else prev_cum_val2

                    prev_cum_val1 = cum_val1
                    prev_cum_val2 = cum_val2

                    difference = abs(cum_val1 - cum_val2)
                    max_difference = max(max_difference, difference)
                    avg_difference += difference/len(rssi_values)
                
            avg_max_summand = max_difference
            avg_difference_summand = avg_difference
                
            avg_max += avg_max_summand
            avg_avgdifference += avg_difference_summand
        
        if jaccard_weighted and jaccard_metric == 1:
            return 30.0, 30.0
        if jaccard_weighted:
            return jaccard_metric*avg_max, jaccard_metric*avg_avgdifference
        else:
            return avg_max, avg_avgdifference
    
    def jaccard_coefficient(self, other):
        bssid_intersection = self.visible_accesspoints().intersection(other.visible_accesspoints())
        bssid_union = self.visible_accesspoints().union(other.visible_accesspoints())
        jaccard_coefficient = 0 if len(bssid_union) == 0 else len(bssid_intersection)/len(bssid_union)
        return jaccard_coefficient
    
    def jaccard_metric(self, other):
        return 1 - self.jaccard_coefficient(other)
    
    def bhattacharyya_multidim(self, other, max_calc_time_s=None):
        '''Calculates the multidimensional Byattacharyya distance of two segments.'''
        x1 = self.normalized_occurences()
        x2 = other.normalized_occurences()

        # Find all the rssi values for the bssids that add something to the Bhattacharya-Distance, i.e. both probabilities greater zero.
        x_values = dict([(bssid, {rssi for rssi in set(x1[bssid].keys()).intersection(set(x2[bssid].keys()))}) for bssid in self.bssids])
        # Remove probabilities where rssi-values are zero.
        x_values_copy = x_values.copy()
        for bssid in x_values:
            if len(x_values[bssid]) == 0:
                del x_values_copy[bssid]
        x_values = x_values_copy

        from itertools import product
        from time import time
        bssid_keys = list(x_values.keys())
        rssi_vals = [x_values[bssid] for bssid in bssid_keys]

        result = 0.0
        ts = time()
        for cross_product_val in product(*rssi_vals):
            current_x = dict([(bssid_keys[i], cross_product_val[i]) for i in range(len(cross_product_val))])
            log_prob1 = self.joint_log_probability(current_x)
            log_prob2 = other.joint_log_probability(current_x)
            joint_prob = math.exp(log_prob1 + log_prob2)
            result += math.sqrt(joint_prob)
            
            # Stop if calculation takes too long.
            if max_calc_time_s is not None and time()-ts > max_calc_time_s: break
        return 1-result
        
    
    def bhattacharyya(self, other):
        '''Calculates the Byattacharyya distance of two segments.'''
        x1 = self.normalized_occurences()
        x2 = other.normalized_occurences()
        rssi_values = list(range(-100, -10))
        
        result = 0.0
        factor = 1/len(self.bssids)
        for bssid in self.bssids:
            x1_bssid, x2_bssid = x1[bssid], x2[bssid]
            summand = 1 - np.sum(np.sqrt([x1_bssid[i]*x2_bssid[i] for i in rssi_values]))
            summand *= factor
            result += summand
        return result