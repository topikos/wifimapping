import numpy as np


def laplace_smoothing(matrix, const=1e-7):
    """ Adds a small constant to every matrix entry and normalizes rows. Returns a new matrix."""
    result = matrix + const
    result *= 1/result.sum(axis=1)[:,np.newaxis] # The indexing is needed to divide each row of the matrix by its sum.
    return result
    
def compute_dist(segm1, segm2, dist_fun, apply_laplace_smoothing=False, laplace_smoothing_const=1e-7, norm='manhattan'):
    """ Generic method to calculate the distance between two WifiSegments.
    Norm can either be "euclidean" or "manhattan".
    """
    p, q = segm1.get_distribution_matrix(), segm2.get_distribution_matrix()
    
    if apply_laplace_smoothing:
        p, q = laplace_smoothing(p, laplace_smoothing_const), laplace_smoothing(q, laplace_smoothing_const)
    
    if norm == 'manhattan':
        # To make the distances more comparable across different experiment environments (and have better interpretability)
        # we normalize the Manhattan norm by the number of APs. For one experiment, this is a constant factor
        # and therefore irrelevant for the overall results.
        result = dist_fun(p, q).sum() / p.shape[0]
    elif norm == 'euclidean':
        result = dist_fun(p, q)
        result = np.dot(result, result)
        result = np.sqrt(result)
    else:
        raise Exception("Unrecognized norm: {}".format(norm))

    return result

def linear_comb_dist(p_mat, q_mat, dist_fun, factors=None, normalize_factors=True):
    """ Takes two matrices where each row represents a discrete probability distribution of a random variable and each column an outcome.
    dist_fun is calculated for each of these rows and a linear combination is built from the resulting distances. 
    Factors are the factors for each row of the matrices. If set to None, each factor is set to 1.
    If normalize_factors is set to true, the factors are normalized such that their sum is 1. """
    assert(p_mat.shape == q_mat.shape)
    
    n_rows = p_mat.shape[0]

    # Set default factors to 1.
    if factors is None:
        factors = np.ones(n_rows)
    
    # Normalize factors such that their sum is 1.
    if normalize_factors:
        factors /= factors.sum()
    
    # Calculate the distance.
    d = 0.0
    for i in range(n_rows):
        p_row, q_row = p_mat[i,], q_mat[i,]
        if not np.array_equal(p_row, q_row):
            d += factors[i] * dist_fun(p_row, q_row)
    return d
    

def crossentropy(p, q):
    """ Calculates the crossentropy. """
    from scipy.special import xlogy
    
    result = -xlogy(p, q)
    # Ignore infinity values. However, the correct way would be to allow this.
    dimension = len(p.shape)
    # If input is a one-dimensional array, we sum over all elements. If it is a matrix, we take the row sums.
    axis = None if dimension == 1 else 1
    result = np.ma.masked_invalid(result).sum(axis=axis)
    return result

def entropy(p):
    """ Calculates the entropy. """
    return crossentropy(p, p)

def kl_divergence(p, q):
    """ Kullback-Leibler (KL) Divergence. """
    return crossentropy(p, q) - entropy(p)

def symmetrized_kl_divergence(p, q):
    """ Symmetrized KL-Divergence """
    return kl_divergence(p, q) + kl_divergence(q, p)

def js_divergence(p, q):
    """ Jenssen-Shannon (JS) Divergence. """
    r = (p + q)*0.5
    return (kl_divergence(p, r) + kl_divergence(q, r))*0.5

def sqrt_js_divergence(p, q):
    """ Square-Root of the JS-Divergence, which is a metric. """
    return np.sqrt(js_divergence(p, q))

def total_variation_distance(p, q):
    """ Total variation distance. """
    dimension = len(p.shape)
    axis = None if dimension == 1 else 1
    return np.abs(p-q).max(axis=axis)

def emd(p, q):
    """ Earth Mover's Distance. """
    dimension = len(p.shape)
    axis = None if dimension == 1 else 1
    return np.abs(p.cumsum(axis=axis)-q.cumsum(axis=axis)).sum(axis=axis)

def bc(p,q):
    """ Bhattacharyya Coefficient """
    dimension = len(p.shape)
    axis = None if dimension == 1 else 1
    return np.sqrt(p*q).sum(axis=axis)

def bd(p,q):
    """ Bhattacharyya Distance """
    b_coeff = bc(p,q)
    # Sometimes a BC is so close too zero that it is rounded to zero.
    # In these cases we replace it by a very small constant, such that the logarithm can still be calculated.
    b_coeff[np.where(b_coeff==0)] = 1e-8
    return -np.log(b_coeff)

def hellinger(p, q):
    """ Hellinger Distance. """
    b_coeff = bc(p,q)
    # Rounding errors can sometimes make the BC very slightly greater than 1.
    # In such cases, we replace it by exactly 1, such that the sqrt can still be calculated (which then returns 0).
    return np.sqrt(1-np.minimum(b_coeff, 1))

def kolmogorov_smirnov(p, q):
    """ Kolmogorov-Smirnov Test Statistic. """
    dimension = len(p.shape)
    axis = None if dimension == 1 else 1
    return np.abs(p.cumsum(axis=axis)-q.cumsum(axis=axis)).max(axis=axis)

def rssi_expectation(p):
    """ Returns a vector which returns the expected value of the rssi for the probability matrix p."""
    rssi_mat = np.array([list(range(-100, -10)) for _ in range(p.shape[0])])
    return np.average(rssi_mat, weights=p, axis=1)

def distribution_mean_euclidean_distance(p, q):
    """ Euclidean Distance between distribution means. """
    p_mean, q_mean = rssi_expectation(p), rssi_expectation(q)
    squared_diffs = np.power(p_mean - q_mean, 2)
    return np.sqrt(squared_diffs.sum())

def distribution_mean_manhattan_distance(p, q):
    """ Manhattan distance between distribution means. """
    p_mean, q_mean = rssi_expectation(p), rssi_expectation(q)
    diffs = np.abs(p_mean - q_mean)
    return diffs.sum()