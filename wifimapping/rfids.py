import pandas as pd
from collections import defaultdict

""" 
DataStructures and functions for working with RFID data.
"""

class ContactSegment():
    """ A segment with a start and end (datetimes)."""
    def __init__(self, start=None, end=None):
        self.start = start
        self.end = end
        
    def __repr__(self):
        return '({}-{})'.format(self.start, self.end)

class Contact():
    """ All contacts from one to another rfid. """
    def __init__(self, source_id=None, destination_id=None):
        self.source_id = source_id
        self.destination_id = destination_id
        self.segments = []
        
    def __repr__(self):
        return 'src: {} dest: {}, segments: {}'.format(self.source_id, self.destination_id, self.segments)
    
def aggregate_contacts(contacts, max_gap_seconds=20):
    """ Aggregate contacts to time segments.
    If there have been two contacts from the same source to the same destination and the time gap between them is lower than 
    or equal to max_gap_seconds, we assume there has been a continous contact during that time window."""
    aggregated_contacts = defaultdict(list)
    
    # Tuple indices of columns in original pandas row. (df.itertuples() is faster than df.iterrows())
    SOURCEID, DESTINATIONID, TS = 1,2,3
    
    # First group by contact source, then the resulting data-frame by contact destination.
    for source_id, contacts_for_source in contacts.groupby('sourceid'):
        for destination_id, contacts_for_destination in contacts_for_source.groupby('destinationid'):
            
            # Create contact for the current source-destination-combo.
            contact = Contact(source_id, destination_id)
            aggregated_contacts[source_id].append(contact)
            
            # Iterate over the contact rows in timestamp order.
            for row in contacts_for_destination.sort_values(by=['ts']).itertuples():
                ts = row[TS]
                
                if len(contact.segments) == 0 or (ts - contact.segments[-1].end).total_seconds() > max_gap_seconds:
                    contact.segments.append(ContactSegment(start=ts, end=ts))
                else:
                    contact.segments[-1].end = ts
    return aggregated_contacts

def aggregate_contacts_bidirectional(contacts, max_gap_seconds=20):
    """ Aggregate contacts to time segments.
    This method takes incoming as well as outgoing contacts for each tag into account (i.e. the tag as source and destination)
    and is therefore more accurate.
    If there have been two contacts from the same source to the same destination and the time gap between them is lower than 
    or equal to max_gap_seconds, we assume there has been a continous contact during that time window."""
    aggregated_contacts = defaultdict(list)
    
    # Tuple indices of columns in original pandas row. (df.itertuples() is faster than df.iterrows())
    SOURCEID, DESTINATIONID, TS = 1,2,3
    
    # First group by contact source, then the resulting data-frame by contact destination.
    tagids = list(set(contacts.sourceid.unique()).union(set(contacts.destinationid.unique())))
    
    for tagid in tagids:
        # Select all incoming and outgoing contacts for tagid (i.e. registered by this tag or a different one).
        src_contacts = contacts.loc[(contacts['sourceid'] == tagid)].copy()
        dest_contacts = contacts.loc[(contacts['destinationid'] == tagid)].copy()
        
        # If this is an incoming contact, switch columns so destinationid always contains the other tags.
        dest_contacts['destinationid'] = dest_contacts['sourceid']
        dest_contacts['sourceid'] = tagid
        
        # Merge data and group by contacts.
        contacts_for_tag = pd.concat([src_contacts, dest_contacts])
        for other_tagid, contacts_with_othertag in contacts_for_tag.groupby('destinationid'):
            
            # Create contact for the current source-destination-combo.
            contact = Contact(tagid, other_tagid)
            aggregated_contacts[tagid].append(contact)
            
            # Iterate over the contact rows in timestamp order.
            for row in contacts_with_othertag.sort_values(by=['ts']).itertuples():
                ts = row[TS]
                
                if len(contact.segments) == 0 or (ts - contact.segments[-1].end).total_seconds() > max_gap_seconds:
                    contact.segments.append(ContactSegment(start=ts, end=ts))
                else:
                    contact.segments[-1].end = ts
    return aggregated_contacts

# Load descriptions of rfids from csv.
rfids = pd.read_csv('rfids.csv')
def get_person_or_place(rfid):
    try:
        return rfids.loc[rfids['id'] == rfid, 'person_or_place'].iloc[0]
    except:
        return str(rfid)

# Pretty print aggregated contacts.
def pretty_print(aggregated_contacts):
    for sourceid, contactlist in aggregated_contacts.items():
        print ('Contacts of {}'.format(get_person_or_place(sourceid)))
        for contact in contactlist:
            print('\t contacts to {}'.format(get_person_or_place(contact.destination_id)))
            for segment in contact.segments:
                print('\t\t{}'.format(segment))

# Convert aggregated contacts to readable data-frame.
def aggregated_contacts_dataframe(aggregated_contacts):
    contacts_list = []
    for contacts in aggregated_contacts.values():
        for contact in contacts:
            source = get_person_or_place(contact.source_id)
            destination = get_person_or_place(contact.destination_id)
            for segment in contact.segments:
                contacts_list.append([source, destination, segment.start, segment.end])
    return pd.DataFrame(contacts_list, columns=['src', 'destination', 'start', 'end'])

def convert_to_ip(readerid):
    """ The ubicon database has tables "sighting" and "contact" each of which have a reference to a "readerid" that recorded them.
    The readerid is actually the ip-adress of the reader and can be used to look up the room in the "room" table.
    Unfortunately the ip-adress is saved wrongly as follows:
    1. Convert each part of the ip to an 8bit (1byte) integer number.
    2. Convert concatenated bytes to signed integer.
    3. Result is a very small negative number.
    This method converts the number, as it is read from the mysql tables sighting/contact, back to the correct IP-Adress.
    Example:  convert_to_ip(-1925994094) -> '141.51.169.146'
    """
    # Convert unsigned integer to byte string
    bin_str = bin((readerid) + 2**32)[2:]
    # There are four times 8 bit each of which represent a part of the ip. 
    bin_list = [bin_str[l:l+8] for l in range(0,4*8,8)]
    # Convert each byte to an integer value.
    ip_parts = [int(part, 2) for part in bin_list]
    # Join to ip-string
    ip = '.'.join([str(part) for part in ip_parts])
    return ip
    