import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from collections import OrderedDict, defaultdict


class FingerPrintCollection():
    """ A wifi fingerprint collection for a single uuid.
    """
    # The underlying wifi data-frame for the uuid.
    df = pd.DataFrame()
    # Data-Frame with one fingerprint for a datetime in each row. Each column represents a bssid.
    X = pd.DataFrame()

    # Bssids and corresponding ssids.
    bssids = dict()
    # Ssids and corresponding bssids
    ssids = defaultdict(set)
    
    def init_from_signal_df(self, df):
        self.X = df
    
    def load_UJIndoorLoc(self):
        import datetime
        def dateparse (time_in_secs):    
            return datetime.datetime.fromtimestamp(float(time_in_secs))
        df = pd.read_csv('UJIndoorLoc/trainingData.csv', parse_dates=['TIMESTAMP'], date_parser=dateparse, index_col='TIMESTAMP')

        # In the csv-file RSS 100 means no measurement. Replace this with np.nan.
        s = slice('WAP001','WAP520')
        self.X = df.loc[:,s].replace(100, np.nan)
        # df.loc[:,s] = df.loc[:,s].replace(100, np.nan)

    def __init__(self, wifi_df=None, uuid=None, missing_val=np.nan, use_all_bssids=False):
        """Initializes the matrix (DataFrame) X of rssi values.
        Each row corresponds to a point in time and each column to a bssid.
        :param wifi_df: The data-frame, from which fingerprints are extracted.
        :param uuid: The uuid from which to extract the fingerprints.
        :param missing_val: How to represent missing values when no rssi exists for a given (datetime, bssid).
        :param use_all_bssids: Create columns for every bssid in wifi_df or only those for the user.
        """
        
        # Also allow no arguments at all for empty FingerPrintCollection
        if wifi_df is None:
            return

        # Copy the part of the underlying wifi-df for this uuid
        self.df = wifi_df.loc[wifi_df['uuid'] == uuid].copy()

        # Create maps {bssid -> ssid} and {ssid -> [bssid_1,...,bssid_n]}
        self.ssids, self.bssids = defaultdict(set), dict()
        df_for_bssids = wifi_df if use_all_bssids else self.df
        for (ssid, bssid), group in df_for_bssids.groupby(['ssid', 'bssid']):
            self.ssids[ssid].add(bssid)
            self.bssids[bssid] = ssid

        # Create matrix indices for every datetime (row) and bssid (column)
        dt_index = OrderedDict([(dt, i) for i, dt in enumerate(self.df.index.unique())])
        bssid_index = OrderedDict([(bssid, i) for i, bssid in enumerate(self.bssids.keys())])

        # Create matrix with default values, e.g. NaN or minimum rssi.
        X = np.zeros((len(dt_index), len(bssid_index)))
        X.fill(missing_val)

        # Fill the matrix where corresponding values exist
        for bssid, group in self.df.groupby(('bssid')):
            if len(group) == 0:
                continue
            col = bssid_index[bssid]
            rssi_series = group['num_val']
            for dt, rssi in rssi_series.iteritems():
                row = dt_index[dt]
                # TODO: Define a "fingerprint-extractor", e.g. signal strength rank or signal strength difference.
                X[row, col] = rssi
                
        # Create a Data-Frame that contains the row- and column-names (datetimes and bssids).
        # This allows to select a column, which is a series, and directly use time series operations.
        # self.X.as_matrix() can be used to get the underlying matrix.
        # Pandas Data-Frame operations can be used.
        self.X = pd.DataFrame(X, columns=bssid_index.keys(), index=dt_index.keys())

    def get_labels(self):
        labels_with_indices = ([(g.index[0], g['label'][0]) for _, g in self.df.groupby('ts')])
        indices, labels = [idx for idx, label in labels_with_indices], [label for idx, label in labels_with_indices]
        return pd.Series(labels, index=indices)

    def get_bssids(self, *ssids):
        """Returns the set of bssids for one or several ssids.
        """
        return set.union(*[self.ssids[ssid] for ssid in ssids])

    def X_with_min_samples(self, min):
        """Returns X for those bssids, that have a minimum of :param min samples.
        """
        return self.X.loc[:, self.X.count() >= min]

    def get_ssid(self, bssid):
        """Returns the unique ssid for a given bssid.
        """
        return self.bssids[bssid]

    def smoothen(self, n_values=6):
        return self.X.rolling(n_values, min_periods=1).median().round(decimals=0)
    
    def n_visible_aps(self):
        return (self.X.isnull()==False).sum(axis=1)

    def replace_missing_vals(self, Y, replacement=-100.0):
        """Returns a copy of the parameter-matrix Y where missing values (np.nan) are replaced by "replacement".
        """
        Y = Y.copy()
        Y[np.isnan(Y)] = replacement
        return Y
        
    def trajectories(self, max_timediff_s=4.0, with_labels=False):
        """ Returns a matrix where each two subsequent fingerprints are concatenated as one row (i.e. two connected fingerprints).
        The trajectory's timestamp is set to be the one of the second fingerprint. The columns of the second step are
        named with a trailing underscore.
        """
        traj_vals = []
        traj_index = []
        if with_labels:
            traj_labels = []
            all_labels = self.get_labels()
        for i in range(0, len(self.X)-1):
            timediff_s = (self.X.index[i+1]-self.X.index[i]).total_seconds()
            if timediff_s < max_timediff_s:
                traj_val = np.concatenate([self.X.iloc[i].values, self.X.iloc[i+1].values])
                traj_vals.append(traj_val)
                traj_index.append(self.X.index[i+1])
                if with_labels:
                    traj_labels.append(all_labels.iloc[i+1])

        traj_cols = np.concatenate([self.X.columns.values, [col + '_' for col in self.X.columns.values]])
        traj_df = pd.DataFrame(traj_vals, columns=traj_cols, index=traj_index)
        return (traj_df, traj_labels) if with_labels else traj_df

    def corr_matrix(self, method='pearson', min_periods=10):
        """ Returns a dataframe of access-point signal-strength correlations.
        """
        bssids = list(self.X.columns.values.copy())
        n_elems = len(bssids)
        corr_matrix = np.matrix(np.zeros(n_elems ** 2).reshape(n_elems, n_elems))

        for i in range(n_elems):
            for j in range(i, n_elems):
                s1, s2 = self.X[bssids[i]], self.X[bssids[j]]
                corr_matrix[i, j] = s1.corr(s2, min_periods=min_periods, method=method)
                corr_matrix[j, i] = corr_matrix[i, j]

        return pd.DataFrame(corr_matrix, columns=bssids, index=bssids)

    def visibility_matrix(self):
        """Returns a DataFrame corresponding to self.X in which every entry is 1 if an access-point is visible and 0 if not.
        """
        return self.X.applymap(lambda x: 0 if np.isnan(x) else 1)
    
    def visibility_probs(self):
        X_vis = self.visibility_matrix()
        X_vis['label'] = self.get_labels()
        grouping = X_vis.groupby('label')
        p_vis = grouping.aggregate(sum)/grouping.aggregate(len)
        return p_vis, grouping.aggregate(len)
    
    def label_probs(self):
        counts = self.get_labels().value_counts()
        return counts / counts.sum()
        
    def learn_plot_embedding(self, alg=None, plot_params=None):
        from sklearn import manifold
        # Example for alg-parameter:
        # alg = manifold.TSNE(n_components=3, init='pca', random_state=0)

        # Replace missing values
        #X, labels = self.X_with_aps()
        X = self.replace_missing_vals(self.X, replacement=-100.0)
        labels = self.X.columns.values
        # TODO: Dummy positions must be concatenated to the embedding (ap_fingerprints() must be implemented).
        # TODO: Labels should also have the ssids.

        # Learn embedding
        if alg is None:
            alg = manifold.MDS(n_components=2, n_init=1, max_iter=100, random_state=0)
        emb = alg.fit_transform(X)

        # Plot the embedding.
        avg_sig_strength = np.mean(X, axis=1)
        min_val = np.min(avg_sig_strength)
        avg_sig_strength = [(val - min_val) for val in avg_sig_strength]
        plot_params = {'marker': '.', 's': avg_sig_strength, 'c': avg_sig_strength, 'alpha': 0.7}
        self.plot_embedding(emb, labels=labels, plot_params=plot_params)
        return emb

    def plot_embedding(self, emb, labels=None, w=9, h=9, plot_params=None):
        """ Plots a 2d or 3d embedding.
        """
        from mpl_toolkits.mplot3d import Axes3D

        # Determine embedding dimension
        is3d = (emb.shape[1] == 3)

        fig = plt.figure(figsize=(w, h))
        ax = fig.add_subplot(111, projection=('3d' if is3d else None))

        # Use average signal strength for size and color
        # TODO: separate function for size and color, automatic normalizing within this method.
        #         avg_sig_strength = [np.mean(x) for x in emb] # FIXME: values have to be from higher dimensional data
        #         min_val = np.min(avg_sig_strength)
        #         avg_sig_strength = [(val-min_val) for val in avg_sig_strength]

        if plot_params is None:
            plot_params = {'marker': '.', 's': 1, 'alpha': 0.7}
            # plot_params = {'marker': '.', 's': avg_sig_strength, 'c': avg_sig_strength, 'alpha': 0.7}
        if is3d:
            par = {**plot_params, **{'xs': emb[:, 0], 'ys': emb[:, 1], 'zs': emb[:, 2]}}
        else:
            par = {**plot_params, **{'x': emb[:, 0], 'y': emb[:, 1]}}
        ax.scatter(**par)

        # Plot the labels
        if labels is not None:
            for i in range(len(labels)):
                x, y = emb[i, 0], emb[i, 1]
                p = (x, y)
                if emb.shape[1] == 3:
                    z = emb[i, 2]
                    p = (x, y, z)
                    ax.text(x, y, z, labels[i])
                else:
                    ax.annotate(labels[i], p)

        plt.show()
