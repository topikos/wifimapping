from .wifimapper import WifiMapper, extract_stationary_wifi_segments, remove_mobile_aps
from .mongo_datasource import MongoDataSource
from .fingerprints import FingerPrintCollection
from .wifisegment import WifiSegment
from .distribution_distances import compute_dist, emd
import numpy as np


def load_ap_locations(locs_file, bssid_file):
    """ Load ground truth locations of access points from CSV file. """
    import pandas as pd

    # Load ground truth locations
    locs = pd.read_csv(locs_file, header=None)
    bssids = pd.read_csv(bssid_file, header=None)
    locs['bssid'] = bssids[1]
    locs.rename(columns={1: 'location'}, inplace=True)
    del locs[0]
    locs.dropna(inplace=True)
    return locs

def strongestLocation(uuid_to_use, wifiDf, locs, location_bssids, start, end):
    """
    Returns the location of the strongest AP from a set of predefined APs/locations.
    """
    
    # Restrict WiFi data to the device and time frame.
    dev_df = wifiDf.loc[wifiDf['uuid'] == uuid_to_use]
    window_df = dev_df.loc[start:end]
    window_df = window_df.loc[window_df.bssid.isin(location_bssids)]
    
    # No data found -> Location is unknown.
    if len(window_df) == 0:
        return None
    
    # Determine which of the predefined APs has sent the strongest signal.
    max_rssi = window_df['rssi'].max()
    max_row = window_df.loc[window_df['rssi'] == max_rssi]
    # TODO: What if several APs have the same strength?
    strongest_ap = max_row.bssid.iloc[0]
    
    # Return the corresponding location.
    strongest_location = locs.loc[locs.bssid == strongest_ap].iloc[0].location
    return strongest_location

def strongestBSSIDForWifiSegment(segment, loc_bssids, strongest_mean = False):
    """
    Determines the strongest access point for a WiFi segment.
    loc_bssids is the set of APs used for localization.
    If strongest_mean is set to true, the AP with the strongest mean RSSI is determined instead of
    the one with a single strongest value.
    """
    import numpy as np
    
    # An AP must have at least RSSI -99. Otherwise a random "invisible ap" would become the ground truth.
    # We would rather have a "None" label in these cases because this can be very misleading.
    max_rssi = -99
    strongestBSSID = None
    strongestBSSIDcounter = dict()
    
    # Get counters for all preset bssids.
    for bssid in loc_bssids:
        if bssid in segment.counts:
            
            # Get maximum RSSI of the segment for the current BSSID.
            rssi_counter = segment.counts[bssid]
            current_is_strongest = False
            
            # Take strongest mean RSSI.
            if strongest_mean:
                weighted_sum = np.sum([rssi*count for rssi, count in rssi_counter.items()])
                current_mean = weighted_sum / np.sum([count for count in rssi_counter.values()])
                if current_mean > max_rssi:
                    current_is_strongest = True
            
            # Take the one strongest RSSI (or the one with a higher count if two have the same strength).
            else:
                current_max = max(rssi_counter.keys())

                # Update strongest BSSID when one RSSI value is stronger
                # or has higher count of the same strength.
                if (current_max > max_rssi) or \
                (current_max == max_rssi and rssi_counter[max_rssi] > strongestBSSIDcounter[max_rssi]):
                    current_is_strongest = True
            
            # Update the strongest RSSI, BSSID and RSSI Counter.
            if current_is_strongest:
                strongestBSSID = bssid
                strongestBSSIDcounter = rssi_counter
                max_rssi = current_mean if strongest_mean else current_max
                
    return strongestBSSID

def locationForBSSID(locs, bssid):
    location = locs.loc[locs['bssid'] == bssid]['location']
    if len(location) > 0:
        return location.iloc[0]
    else:
        return "Unknown"


class MappingController():

    def __init__(self, params=None):
        self.mappers = {}

        # Set default parameters.
        self.params = { 
            'mongourl': 'mongodb://localhost:27017/',
            'locs_file': '/path/to/locations.csv',
            'bssid_file': '/path/to/bssids.csv',
            'drop_duplicates': True,
            'remove_zero_rssi': True,
            'count_invisible_aps': True,
            'missing_val_representation': -100.0,
            'stationarity_threshold_ms': 10*1000,
            'distribution_types': 'pmf',
            'distance_function': emd,
            'laplace_smoothing': False,
            'remove5GHz': True,
            'classify_outliers': True,
            'strongestMean': True # Use strongest mean RSSI for determination of ground truth location. Otherwise strongest RSSI is used.
        }
        # Overwrite where different.
        if params is not None:
            for param, value in params.items():
                self.params[param] = value
    
    def load_data(self, start, end):
        datasource = MongoDataSource(url=self.params['mongourl'])
        # Some smartphone model (S8) was observed to report 0 RSSI values. We remove these.
        datasource.drop_duplicates = self.params['drop_duplicates']
        datasource.remove_zero_rssi = self.params['remove_zero_rssi']
        wifiDf, motionDf = datasource.fetch_interval(start, end)
        print("UUIDS: {}".format(wifiDf.uuid.unique()))
        # FIXME: This can remove data of interest when the timezone of the server does not match the timezone of the device.
        #self.wifiDf = wifiDf.loc[start:end]
        self.wifiDf = wifiDf
        self.motionDf = motionDf

        # The FingerPrintCollection expects a "num_val" column for rssi-values. Therefore we rename it.
        self.wifiDf.rename(columns={"rssi": "num_val"}, inplace=True)

        # Remove 5GHz APs.
        if self.params['remove5GHz']:
            print("Removing 5GHz Access Points.")
            self.wifiDf = self.wifiDf.loc[self.wifiDf['frequency']<5000]

        # Load Location data.
        self.locs = load_ap_locations(self.params['locs_file'], self.params['bssid_file'])
        self.location_bssids = set(self.locs.bssid)

    def init_mapper(self, uuid):

        mapper = WifiMapper(user=uuid, uuid=uuid, cluster_method='hdbscan')
        missing_val_representation = self.params['missing_val_representation'] if self.params['count_invisible_aps'] else np.nan
        mapper.fingerprints = FingerPrintCollection(wifi_df=self.wifiDf, uuid=uuid, use_all_bssids=True, missing_val=missing_val_representation)
        # The matrix in the FingerPrintCollection is not initialized with a DateTimeIndex.
        # The index is, however, needed to select date ranges.
        import pandas as pd
        mapper.fingerprints.X.index = pd.to_datetime(mapper.fingerprints.X.index)

        # Init motion data. MotionData has a "label" instead of "motionMode". MotionModes are represented through numbers instead of strings.
        mapper.movement_segments = self.motionDf.loc[self.motionDf.uuid == uuid].copy()
        mapper.movement_segments['label'] = mapper.movement_segments['motionMode'].apply(lambda mode: 1 if mode == "MOVING" else 0)

        threshold_ms = self.params['stationarity_threshold_ms']
        mapper.wifi_sets = extract_stationary_wifi_segments(mapper.movement_segments, mapper.fingerprints, stationary_threshold_milliseconds=threshold_ms)
        mapper.wifi_segments = [WifiSegment(wifi_set, bssids=mapper.fingerprints.bssids, distribution_types=self.params['distribution_types']) for wifi_set in mapper.wifi_sets]

        # Determine ground truth locations from ESP32 data.
        predicted_segment_locations = [locationForBSSID(self.locs, strongestBSSIDForWifiSegment(segment, self.location_bssids, strongest_mean=self.params['strongestMean'])) for segment in mapper.wifi_segments]
        mapper.gt_positions = [[pos] for pos in predicted_segment_locations]
        return mapper
    
    def init_mappers(self, uuids=None):
        mappers = []
        uuids = self.wifiDf.uuid.unique() if uuids is None else uuids
        # Remove uuis not contained in the data.
        uuids = [uuid for uuid in uuids if uuid in self.wifiDf.uuid.unique()]
        for uuid in uuids:
            mappers.append(self.init_mapper(uuid))
        return mappers

    def run_aggregate_mapper(self, uuids=None):
        mappers = self.init_mappers(uuids)
        totalMapper = AggregateMapper(mappers,
                     distribution_types=self.params['distribution_types'], 
                     classify_outliers=self.params['classify_outliers'])

        self.totalMapper = totalMapper

        dist = self.params['distance_function']
        dist_function = lambda a, b: compute_dist(a,b, dist, apply_laplace_smoothing=self.params['laplace_smoothing'])
        totalMapper.compute_distance_matrix(dist_function)
        totalMapper.calculate_coordinates()
        totalMapper.cluster_coordinates()
        return totalMapper


class AggregateMapper(WifiMapper):
    """
    While a default WiFiMapper creates a topological map for a single device/user,
    an AggregateMapper learns a map for all these users together.
    At the moment, the mappers are expected to have extracted WiFiSegments already.
    The AggregateMapper can then calculate distances, coordinates and do clustering.
    """
    
    def __init__(self, mappers, distribution_types='pmf', cluster_method='hdbscan', classify_outliers=False):
        
        # Call super constructor.
        super().__init__(user="All", uuid=None,
                         distribution_types=distribution_types,
                         cluster_method=cluster_method,
                         classify_outliers=classify_outliers) 
        
        self.mappers = mappers
        self.wifi_segments = []
        self.gt_positions = []
        
        for m in self.mappers:
            self.wifi_segments += m.wifi_segments
            self.gt_positions += m.gt_positions
