import numpy as np
import pandas as pd
from urllib.request import urlopen
import matplotlib.pyplot as plt
import json
import time
from datetime import datetime


# Contains various methods to request and handle data from the EveryAware platform.

def get_sources(feed_name):
    url = 'https://cs.everyaware.eu/api/v1/finalfeeds/{}/sources?offset=0&limit=100&detailed=false'.format(feed_name)
    response = urlopen(url)
    data = json.loads(response.read())
    return data

def get_newest_sources(feed_name):
    result = sorted([(src.get('lastUpdated'), src.get('sourceId')) for src in get_sources(feed_name)], reverse=True)
    # Workaround for wrong timestamps in the sources list for some devices.
    return list(filter(lambda x: x[0]//10**13==0, result))

def get_newest_source_id(feed_name):
    return get_newest_sources(feed_name)[0][1]

def get_all_data(feed_name, source_id):
    firstTS = 0
    lastTS = int(time.time()*1000)
    return get_data(feed_name, source_id, firstTS=firstTS, lastTS=lastTS)
    
def get_data(feed_name, source_id, seconds=60, firstTS=None, lastTS=None):
    if firstTS is None or lastTS is None:
        if seconds is None:
            raise Exception("You must provide either the number of seconds or a first and last timestamp.")
        lastTS = int(time.time()*1000)
        firstTS = lastTS-1000*seconds
    else:
        convert_datetime = lambda dt: int(dt.timestamp()*1000)
        if type(firstTS) is datetime:
            firstTS = convert_datetime(firstTS)
        if type(lastTS) is datetime:
            lastTS = convert_datetime(lastTS)
            
    url = 'https://cs.everyaware.eu//api/v1/data/minimalpoint?sourceId={}&feed={}&firstTS={}&lastTS={}' \
            .format(source_id, feed_name, firstTS, lastTS)
    print(url)
    response = urlopen(url)
    data = json.loads(response.read())
    return data

def print_data(data):
    for datapoint in data.get('data'):
        print(datapoint)
        print(datapoint.get('timestamp'))
    
        for sensor, sensorcomponents in datapoint.get('channels').items():
            print(sensor)
            for comp_name, comp_val in sensorcomponents.items():
                print(comp_name)
                print(comp_val)
            #print(datapoint.get('channels').get(channel))
        break

def save_json(data, filename):
    """ Save json data to file."""
    with open(filename, 'w') as f:
        f.write(json.dumps(data))
    print('Json data saved to ' + filename)

def load_json(filename):
    with open(filename, 'r') as f:
        return json.loads(f.read())
    
def data_to_df(data, sensorname, lowcase_cols=True, src_id=None):
    from datetime import datetime
    from collections import defaultdict
    
    # Initialize column lists for the resulting data-frame.
    cols = defaultdict(list)
    
    # Iterate over the whole data to find all column names. Otherwise we cannot handle missing values.
    col_names = set()
    for datapoint in data.get('data'):
        for sensor, sensorcomponents in datapoint.get('channels').items():
            if sensor == sensorname:
                for comp_name, comp_val in sensorcomponents.items():
                    col_names.add(comp_name)

    # Add values from json-data to each column.
    for datapoint in data.get('data'):

        for sensor, sensorcomponents in datapoint.get('channels').items():
            if sensor == sensorname:
                # Data
                for comp_name in col_names:
                    # If a value is missing, this will be set to None.
                    comp_val = sensorcomponents.get(comp_name)
                    col_name = comp_name.lower() if lowcase_cols else comp_name
                    cols[col_name].append(comp_val)

                # Timestamps
                ts = datapoint.get('timestamp')
                cols['ts'].append(ts)
                cols['dt'].append(datetime.fromtimestamp(ts/1000))

    dt = cols['dt']
    del cols['dt']
    cols['uuid'] = len(dt)*[src_id]
    df = pd.DataFrame(cols, index=dt)
    return df

def data_to_wifi_df(data):
    return data_to_df(data, 'wifi', lowcase_cols=True).rename(columns={'level': 'num_val'})

def align(s, min_allowed_diff=100):
    result = []
    prevVal = s.iloc[0]
    for idx, val in s.items():
        diff = val - prevVal
        if diff > 0 and diff < min_allowed_diff:
            val = prevVal
        result.append(val)
        prevVal = val
    return result

def realign_df(df, min_allowed_diff=100):
    """ Returns a new DataFrame where the timestamp column ('ts') and the DateTimeindex
    are realigned so that consecutive values with a difference lower than min_allowed_diff
    have now the same value. The first value/oldest date is taken as a replacement.
    """
    from datetime import datetime
    
    aligned = align(df['ts'])
    dts = [datetime.fromtimestamp(ts/1000) for ts in aligned]
    res = df.set_index(pd.DatetimeIndex(dts))
    res['ts'] = aligned
    return res

def plot_df(df, with_legend=True, rollingmax=False, remove5ghz=False):
    plt.figure()
    n_kinds = len(df['bssid'].unique())
    hsv = plt.get_cmap('hsv')
    colors = hsv(np.linspace(0, 1.0, n_kinds))
    i=0
    for name, g in df.groupby('bssid'):
        ssid = str(g['ssid'][0])
        chan = g.frequency[0]
        curve_label = ssid + " " + name + " " +str(chan)
        
        if remove5ghz and not str(chan).startswith('2'):
            continue
            
        if rollingmax:
            g['num_val'].rolling(6).max().plot(label=curve_label, color=colors[i])
            g['num_val'].rolling(6).median().plot(label=curve_label, color=colors[i])
        else:
            g['num_val'].plot(label=curve_label, color=colors[i])
        i+=1
    if with_legend:
        plt.legend()
        
def count_values(data, sensorname):
    count = 0
    for datapoint in data.get('data'):
        for sensor, sensorcomponents in datapoint.get('channels').items():
            if sensor == sensorname:
                count += 1
    return count