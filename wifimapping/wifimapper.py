from .wifisegment import WifiSegment
from .fingerprints import FingerPrintCollection
from .helpers import series_for
import numpy as np
import pandas as pd
from datetime import timedelta
import pickle


def remove_mobile_aps(wifi_df):
    ''' Returns a WiFi DataFrame which is cleaned from mobile aps contained in a blacklist of prefixes and postfixes. '''
    forbidden_prefixes = ['Alcatel Onetouch', 'AndroidAP', 'AndroidHotspot', 'Audi',
                      'Audi_MMI', 'BMW', 'CAR_', 'California 4motion TDI', 'HUAWEI', 'Honor ',
                     'My BMW Hotspot', 'Porsche', 'Skoda', 'VW', 'VodafoneMobileWiFi', 'Volkswagen',
                      'WiFi Hotspot', 'iPhone', 'porsche', 'Music Block', 'BLOCK SB100', 'PS4', 'Playstation', 'Belkin', 'Bose', 'ZYXEL', 'YOURSTYLE', 'FlixBus']
    forbidden_postfixes = ['iPhone']
    for prefix in forbidden_prefixes:
        indices_to_drop = wifi_df.ssid.str.startswith(prefix)
        wifi_df = wifi_df.loc[indices_to_drop == False]
    for postfix in forbidden_postfixes:
        indices_to_drop = wifi_df.ssid.str.endswith(postfix)
        wifi_df = wifi_df.loc[indices_to_drop == False]
    return wifi_df

def classify_movement_mode(acceleration_df, windowsize_ms=3000, windowmovement_ms=1500):
    ''' Returns a series of windows classified by movement mode. (1 for movement, 0 for static). '''
    from .helpers import timestamp_diffs

    def energy(signal, windowsize_ms=2560, windowmovement_ms=1280):
        offset = int(windowmovement_ms*np.median(timestamp_diffs(signal))/windowsize_ms)
        # Take every nth value (with n being the offset between two windows)
        return (signal*signal).rolling('{}ms'.format(windowsize_ms)).mean()[offset::offset]

    def variance(signal, windowsize='2s560ms', windowmovement_ms=1280):
        #offset = int(windowmovement_ms/np.mean(timestamp_diffs(signal)))
        offset = 25
        return signal.rolling(windowsize).var()[offset::offset]
    
    movement_df = ((energy(acceleration_df['abs'], windowsize_ms=windowsize_ms, windowmovement_ms=windowmovement_ms)>106.7897)*1)
    return movement_df

def aggregate_movement(series, max_gap_ms=5000, window_size_ms=2560):
    ''' Aggregates a time-indexed series of classification values.
    All consecutive windows with the same classification value are summarized as one time segment in the resulting DataFrame. '''
    from datetime import timedelta
    import numpy as np
    
    starts = []
    ends = []
    labels = []
    
    i = 0
    while i < len(series):
        # Determine initial segment start/end for the time window.
        segment_end = series.index[i]
        segment_start = segment_end# - timedelta(milliseconds=window_size_ms)
        segment_label = series.iloc[i]
        
        # Enlarge the segment as long as the label for the next time window is the same.
        # Do this only, if the time-gap is not too big.
        while i+1 < len(series) and series.iloc[i+1] == segment_label and (series.index[i+1]-series.index[i]) < timedelta(milliseconds=max_gap_ms):
            segment_end = series.index[i+1]
            i += 1
        
        starts.append(segment_start)
        ends.append(segment_end)
        labels.append(segment_label)
        i+=1
    return pd.DataFrame(np.column_stack([starts, ends, labels]), columns=['start', 'end', 'label'])

def extract_stationary_wifi_segments(movement_df, wifi_fingerprints, stationary_threshold_milliseconds = 1000*10):
    """ Extracts sets of Wifi data that are known to belong to a single place, since there was no movement inbetween.
    Segments are extracted if they are at least 'stationary_threshold_milliseconds' long.
    TODO: Using the time thresholds, we don't really know what occurs between segments or somehow have to reconstruct 
    whether there is a gap inbetween. We will need to be more sure to infer about the connections between places. """
    result = []
    
    for row in movement_df.iterrows():
        row = row[1]

        # If we have a movement segment, we don't care for the WiFi data at the moment.
        if row.label == 1:
            continue

        # Otherwise we calculate the duration of the window. It is above a given threshold, we select its WiFi data.
        duration = row.end-row.start
        if duration > timedelta(milliseconds=stationary_threshold_milliseconds):
            matrix_for_window = wifi_fingerprints.X.loc[row.start:row.end]
            
            # There might have been no Wifi data during the time window (generally should not happen,
            # since we recorded with same device, however it can occasionally happen).
            if len(matrix_for_window) == 0:
                continue
                
            fp_for_window = FingerPrintCollection()
            fp_for_window.init_from_signal_df(matrix_for_window)
            result.append(fp_for_window)
    return result

def compute_distance_matrix(wifi_segments, dist_function=lambda segm1, segm2: segm1.total_variance(segm2), symmetric=True, positive_definite=True):
    ''' Takes a list of WifiSegments and computes the quadratic distance matrix between all of them.
        If symmetric is set to true, only half of the matrix elements will be calculated. If positive_definite is true, all diagonal elements will be set to zero. '''
    n = len(wifi_segments)
    distances = np.zeros((n, n))
    for i in range(n):
        for j in range(0 if not symmetric else i, n):
            if positive_definite and i==j: continue
            distances[i, j] = dist_function(wifi_segments[i], wifi_segments[j])
            if symmetric:
                distances[j, i] = distances[i, j]
    return distances

def get_ground_truth_position(contacts_df, timeframe, user):
    """ Returns the rfid location(s) for a given timeframe (start, end) in the order of their occurrence. """
    
    def has_interval_overlap(start1, end1, start2, end2):
        """ Returns true, if two intervals overlap. """
        return (start1 <= start2 and end1 > start2) or (start1 < end2 and end1 >= end2) or (start1 == start2 and end1 == end2)
    def apply_intersection(contacts_row):
        """ Function to apply the interval overlap function to the pandas DataFrame."""
        return has_interval_overlap(contacts_row['start'], contacts_row['end'], timeframe[0], timeframe[1])
        
    # Find all positions of the user.
    user_positions = contacts_df.loc[(contacts_df['src'] == user)]
    # Find positions for the given timeframe.
    positions = user_positions.loc[user_positions.apply(apply_intersection, axis=1)]
    # Join all positions with overlap
    return list(positions['destination'])

def get_ground_truth_positions(contacts_df, timeframes, user):
    ''' Returns a tuple, (gt_positions, location_indices). gt_positions is a list of all the ground truth positions of a user.
    location_indices is a map of ground truth locations to sets of segment indices.'''
    # TODO: only use "true" contacts (i.e. without contact aggregation)

    # Determine all ground truth positions (also includes contacts to other people).
    from collections import defaultdict
    gt_positions = []
    location_indices = defaultdict(set)
    for i in range(len(timeframes)):
        position = get_ground_truth_position(contacts_df, timeframes[i], user)
        gt_positions.append(position)

        for pos in position:
            location_indices[pos].add(i)
    return gt_positions, location_indices

def calculate_transition_probabilities(wifi_segments, normalization_method='probabilities'):
    ''' Returns a defaultdict of transition probabilities between segments.
    The given list of segments is assumed to be ordered in time and a visit of wifi_segments[i+1] to follow on wifi_segments[i].
    normalization_method is either 'maximum' or 'probabilities'. '''
    from collections import defaultdict, Counter

    # Count transitions and determine maximum count of all transitions.
    transit_probs = defaultdict(Counter)
    max_transit_count = 0
    for i in range(len(wifi_segments)-1):
        segmA, segmB = wifi_segments[i], wifi_segments[i+1]
        if segmA.location != segmB.location:
            transit_probs[segmA.location][segmB.location] += 1
            max_transit_count = max(max_transit_count, transit_probs[segmA.location][segmB.location])
        
    # Normalize either to probabilities (i.e. sum for ) or otherwise by the maximum transition count.
    normalize = "maximum"
    normalize = "probabilities"
    for locationA in transit_probs:
        current_probs = transit_probs[locationA]
        count_sum = sum(current_probs.values())
        for locationB in current_probs:
            if normalize == "probabilities":
                current_probs[locationB] /= count_sum
            elif normalize =="maximum":
                current_probs[locationB] /= max_transit_count
        
    return transit_probs

def save_map(wifi_segments, transit_probs, filename='./place_animation_website/mapdata.json'):
    ''' Saves the learned map to a json-file containing all segments, ground truths and transition probabilities.
        TODO: The format is java-script, not json.
        TODO: Change this for usage in a package.
    '''
    def segment_to_json(segment):
        timeframe = segment.timeframe
        def get_timestamp(time):
            return int(time.timestamp()*1000)
        return "{" + '\"start\": {}, \"end\": {}, \"place\": {}, \"ground truth\": \"{}\"'.format(get_timestamp(timeframe[0]), get_timestamp(timeframe[1]), segment.location, ','.join(segment.gt_positions)) + "}"
    
    def segments_to_json(segments):
        return '[{}]'.format(','.join([segment_to_json(segment) for segment in segments]))

    def transit_probs_to_json(transit_probs):
        return '{{ {} }}'.format(', '.join([str(i) + ": " + repr(dict(transit_probs[i])) for i in transit_probs]))

    # Save the learned map to a json-file.
    with open(filename, 'w') as file:
        file.write("mapdata={};\n".format(segments_to_json(wifi_segments)))
        file.write("transitprobs={};".format(transit_probs_to_json(transit_probs)))
    print("Exported mapdata.")

class WifiMapper():

    def __init__(self, user, uuid, distribution_types='pmf', cluster_method='hdbscan', classify_outliers=True):
        ''' distribution_types are according to the WiFiSegment class and can be either "pmf", "kde" or "normal".
        cluster_method can be either 'hdbscan' or 'bayesiangmm'.
        '''
        self.user = user
        self.uuid = uuid
        self.fingerprints = None
        self.count_invisible_aps = True
        # Maps location labels obtained as ground truth to numbers.
        self.gt_locations_map = dict()
        self.distribution_types = distribution_types
        self.cluster_method = cluster_method
        self.gt_locations = None
        self.classify_outliers = classify_outliers
    
    def set_count_invisible_aps(self, count_invisible_aps):
        ''' If set to True, when WiFi segments are extracted also the probability of missing an access point's beacon is estimated.
        Experimentally, this led to better results.'''
        self.count_invisible_aps = count_invisible_aps
    
    def set_data_source(self, wifi_df, sensor_data, contacts_df=None, reset_data_source=False, use_all_bssids=False):
        ''' Sets the data sources (wifi and sensor data) and extracts the data that is relevant for the person.
        Set reset_data_source=True to release the full data source. This is especially relevant for parallelization. '''
        self.wifi_df = wifi_df
        self.sensor_data = sensor_data
        self.contacts_df = contacts_df
        self._extract_personal_data(reset_data_source, use_all_bssids=use_all_bssids)

    def _extract_personal_data(self, reset_data_source=False, use_all_bssids=False, missing_val_rssi=-100.0):
        # Set each fingerprint entry for a timestamp to -100.0, if we want to also estimate the probability that an access point's beacon is missed.
        missing_val_representation = missing_val_rssi if self.count_invisible_aps else np.nan
        self.fingerprints = FingerPrintCollection(wifi_df=self.wifi_df, uuid=self.uuid, use_all_bssids=use_all_bssids, missing_val=missing_val_representation)
        self.acceleration_df = series_for(df=self.sensor_data, sensor='Accelerometer', uuid=self.uuid)
        self.acceleration_df = pd.DataFrame(self.acceleration_df).sort_index()
        self.acceleration_df['abs'] = np.sqrt(self.acceleration_df['accX']**2+ self.acceleration_df['accY']**2 + self.acceleration_df['accZ']**2)
        if reset_data_source:
            self.wifi_df = None
            self.sensor_data = None
    
    def run(self):
        if self.fingerprints is None or self.acceleration_df is None:
            print('Error: Must set the data source first.')
            return
        
        self.classify_movement_mode()

        # TODO: This releases the acceleration df such that the there is more memory left. Movement recognition can, however, not be repeated after this. 
        self.acceleration_df = None

        self.extract_stationary_wifi_segments()
        self.determine_gt_labels()
        self.compute_distance_matrix()
        self.calculate_coordinates()
        self.cluster_coordinates()
        self.calculate_transition_probabilities()
        self.save_map()
        self.plot_segment_clustering()

    def classify_movement_mode(self, savefolder='./'):
        print('Classifying movement modes.')
        self.movement_mode_series = classify_movement_mode(self.acceleration_df, windowsize_ms=3000, windowmovement_ms=1500)
        self.movement_segments = aggregate_movement(self.movement_mode_series, max_gap_ms=5000)
        filename = savefolder + 'aggregated_movement_{}_{}.csv'.format(self.user, self.uuid)
        print('Saving movement modes to {}'.format(filename))
        self.movement_segments.to_csv(filename, index=False)
    
    def extract_stationary_wifi_segments(self, threshold_ms=10*1000):
        print('Extracting Wifi segments')
        self.wifi_sets = extract_stationary_wifi_segments(self.movement_segments, self.fingerprints, stationary_threshold_milliseconds=threshold_ms)
        self.reinit_wifi_segments()
    
    def reinit_wifi_segments(self):
        ''' Reinitialize WiFiSegments. This method is useful when e.g. the distribution_types property has changed. '''
        self.wifi_segments = [WifiSegment(wifi_set, bssids=self.fingerprints.bssids, distribution_types=self.distribution_types) for wifi_set in self.wifi_sets]
        self.determine_gt_labels()
    
    def compute_distance_matrix(self, dist_function=lambda segm1, segm2: segm1.earth_movers_distance(segm2), symmetric=True, positive_definite=True):
        print('Computing segment distances')
        self.segment_distances = compute_distance_matrix(self.wifi_segments, dist_function, symmetric=symmetric, positive_definite=positive_definite)

    def calculate_coordinates(self, n_components=2):
        # Apply MDS (multi-dimensional scaling) to the distance matrix to get coordinates for each segment.
        # Per default we use 2d-coordinates, which allow for good visualization. If a dataset stems from a building with several floors, 3d-coordinates are recommended.
        print('Calculating MDS coordinates.')
        from sklearn.manifold import MDS
        self.coordinates = MDS(n_components=n_components, metric=True, dissimilarity='precomputed').fit_transform(self.segment_distances)
    
    def _cluster_function(self, coordinates):
        ''' Returns cluster labels for the given segment coordinates.
        Overwrite this method to use a different clustering method. The default is a GMM using Variational Bayes, which automatically determines the number of clusters.
        '''
        from sklearn.mixture import BayesianGaussianMixture
        gmm = BayesianGaussianMixture(n_components=20, covariance_type='full').fit(coordinates)
        return gmm.predict(coordinates)
    
    def hdbscan_cluster(self, coordinates):
        from hdbscan import HDBSCAN
        clusterer = HDBSCAN(min_cluster_size=5)
        labels = clusterer.fit(coordinates).labels_

        if self.classify_outliers:
            # Also try to classify the noise. First train a classifier on the cluster labels which are not from outliers.
            classified_indices = [i for i, label in enumerate(labels) if label != -1]
            if len(classified_indices) == 0:
                return labels # Everything is an outlier -> Cannot classify further.

            from sklearn.neighbors import KNeighborsClassifier
            knn = KNeighborsClassifier(n_neighbors=3).fit(coordinates[classified_indices], labels[classified_indices])
            
            # Then predict labels for the outliers.
            for i, label in enumerate(labels):
                if label == -1:
                    labels[i] = knn.predict([coordinates[i]])
        
        return labels

    def cluster_coordinates(self):
        ''' Performs a clustering and sets cluster labels for all segments. Returns the determined labels. '''
        if self.cluster_method == 'hdbscan':
            self.labels = self.hdbscan_cluster(self.coordinates)
        else:
            self.labels = self._cluster_function(self.coordinates)

        # Add cluster label to the segments.
        for i, label in enumerate(self.labels):
            self.wifi_segments[i].location = label
        return self.labels

    def determine_gt_labels(self, gt_filter_set=set()):
        '''
        Retrieves ground truth labels for the segments (rfid contacts with time overlap) and adds them to the segments.
        '''
        print('Retrieving ground truth locations.')
        if self.contacts_df is None:
            print('ERROR: No ground truth data available. Did you set the contacts_df property?')
            return

        self.gt_positions, self.location_indices = get_ground_truth_positions(self.contacts_df, [segment.timeframe for segment in self.wifi_segments], self.user)
        for i, gt in enumerate(self.gt_positions):
            self.wifi_segments[i].gt_positions = gt
        
        self.replace_gt_by_numbers(filterset=gt_filter_set)

    def replace_gt_by_numbers(self, filterset=set()):
        """ Replaces each ground truth label by a number and saves it to segments as a property "new_gt".
            The given filterset are labels that should be removed from the ground truth (e.g. contacts to other persons). """
        for segment in self.wifi_segments:
            new_gt = []
            for gt in segment.gt_positions:
                
                # Remove gt that should be filtered out
                if gt in filterset:
                    continue
                # Add index for previously unseen gt.
                if gt not in self.gt_locations_map:
                    self.gt_locations_map[gt] = str(len(self.gt_locations_map))
                    
                # Append index to result list (with replaced gt labels)
                new_gt.append(self.gt_locations_map[gt])
            segment.new_gt = new_gt

    def calculate_transition_probabilities(self):
        print('Calculating transition probabilities.')
        self.transit_probs = calculate_transition_probabilities(self.wifi_segments, normalization_method='probabilities')
    
    def save_map(self):
        # Save the learned map.
        print('Saving the map.')
        if self.wifi_segments is None or self.transit_probs is None:
            print('Error: Cannot save the map before determining wifi segments and calculating transition probabilities.')
        save_map(self.wifi_segments, self.transit_probs)
    
    def location_log(self):
        """
        Returns a DataFrame containing the visited locations with time intervals (determined from ground truth).
        """
        assert(len(self.gt_positions) == len(self.wifi_segments))
        n_rows = len(self.gt_positions)
        
        rows = []
        for i in range(n_rows):
            location = self.gt_positions[i][0]
            start = self.wifi_segments[i].timeframe[0]
            end = self.wifi_segments[i].timeframe[1]
            duration = end - start
            rows.append([location, start, end, duration])
        return pd.DataFrame(rows, columns=["location", "start", "end", "duration"])
    
    def plot_segment_clustering(self, show=False, labels='numbers', savefolder='./'):
        '''
        Plots the segment clustering and saves it to a file. If show is True, the plot will be displayed.
        Set savefolder to "None" to disable saving.
        '''
        import matplotlib.pyplot as plt
        import seaborn as sns
        sns.set()
        sns.set_context('paper')

        plt.figure(figsize=(10,10))
        plt.scatter(self.coordinates[:,0], self.coordinates[:,1], s=5, c=self.labels, cmap=plt.get_cmap('Set1'))
        if self.gt_positions is not None:
            for i, (x, y) in enumerate(self.coordinates):
                if len(self.gt_positions[i]) > 0:
                    plot_labels = self.wifi_segments[i].new_gt if labels == 'numbers' else self.gt_positions[i]
                    label_text = ','.join(plot_labels)
                    plt.text(x, y, label_text)
        else:
            print('No ground truth data available. Will not plot any ground truths.')
        
        if savefolder is not None:
            filename = savefolder + 'mds_segment_clustering_{}_{}.pdf'.format(self.user, self.uuid)
            print('Saving clustering plot to {}.'.format(filename))
            plt.savefig(filename)
        if show:
            plt.show()
    
    def plot_segment_clustering3d(self):
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        import seaborn as sns
        sns.set()
        sns.set_context('paper')

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(self.coordinates[:,0], self.coordinates[:,1], self.coordinates[:,2], s=5, c=self.labels, cmap=plt.get_cmap('Set1'))

        for i, (x, y, z) in enumerate(self.coordinates):
            plot_labels = self.gt_positions[i]
            label_text = ','.join(plot_labels)
            ax.text(x, y, z, label_text[:1]) # Shortened labels for better overview in 3d.
        plt.show()


def load_data_sources(basefolder='./office_dataset/'):
    # Load and clean Wifi data
    wifi_df = pd.read_pickle(basefolder + 'wifi_data_complete.gz')
    wifi_df = remove_mobile_aps(wifi_df)

    # Load smartphone/person associations (for association to rfids at gt-locations and contacts to other persons).
    smartphones_df = pd.read_csv(basefolder + 'smartphones.csv')

    # Load the sensor data containing the accelerations of all smartphones.
    sensor_data = pd.read_pickle(basefolder + 'sensor_data_complete_pickle.gz')
    #sensor_data.index = sensor_data['dt']

    # Load RFID contacts.
    contacts_df = pd.read_csv(basefolder + 'aggregated_contacts.csv', parse_dates=['start', 'end'])
    return {
        'wifi': wifi_df,
        'smartphones': smartphones_df,
        'sensors': sensor_data,
        'contacts': contacts_df
    }


if __name__ == '__main__':

    data_source = load_data_sources()
    mapper = WifiMapper('Bob', 'someuuid')
    mapper.set_data_source(data_source['wifi'], data_source['sensors'], data_source['contacts'])
    mapper.run()