# WiFiMapping
Through this package you can create topological maps from recorded WiFi and motion data, typically obtained through a smartphone.

# How to use the package

## Code examples
Please have a look at the [jupyter notebooks](https://gitlab.vgiscience.de/topikos/wifimapping/blob/master/jupyter%20notebooks/) for examples.
## Data format
This package heavily relies on pandas DataFrames. A WiFi DataFrame looks as follows:

timestamp | uuid | ssid | bssid | num_val | frequency
--- | --- | --- | --- | --- | ---
2019-06-07 14:32:50.654 | 998ec5f7-71b4-4784-8779-bc7e2472b843 | ESP_FDC75D | 80:7d:3a:fd:c7:5d	| -60 | 2412
2019-06-07 14:32:50.654 | 998ec5f7-71b4-4784-8779-bc7e2472b843 | ESP_FDE7ED | 80:7d:3a:fd:e7:ed | -80 | 2412
2019-06-07 14:32:50.654 | 998ec5f7-71b4-4784-8779-bc7e2472b843 | ESP_FDE1CD | 80:7d:3a:fd:e1:cd | -83 | 2412
2019-06-07 14:32:53.695 | 998ec5f7-71b4-4784-8779-bc7e2472b843 | ESP_FDC75D | 80:7d:3a:fd:c7:5d	| -57 | 2412
2019-06-07 14:32:53.695 | 998ec5f7-71b4-4784-8779-bc7e2472b843 | ESP_FDE7ED | 80:7d:3a:fd:e7:ed | -74 | 2412

Each row in the DataFrame is a sensor measurement. The *timestamp* is a DatatimeIndex for the DataFrame and contains the timestamp at which a measurement was done. Several measurements can have the same timestamp. The *uuid* is a unique identifier for the device which did the measurement. The *ssid* is the WiFi network ssid (the network name), *bssid* is the mac-address of the access point (AP). The *num_val* contains the sensor value (here the received signal strength indicator or RSSI). The *frequency* is the frequency band of the AP.

Data from other formats (e.g., json) are converted into this format.

# How to install
## Cloning the repository
- Clone the repository to an arbitrary place on your local disk
- To be able to import the packages and modules from anywhere proceed as follows:
    - Find out where site-packages are stored on your disk for the current python environment: `python -m site --user-site`
    - Switch to the folder returned by the previous command
    - Create a PYTHONPATH file with an arbitrary name, e.g.,"wifimapper.pth"
    - Insert the path to your locally cloned repository into the PYTHONPATH file
    - You should now be able to import the package or its modules from anywhere, provided you are in the same python environment.
## Through pip
- Coming soon.
